/**
 * Created by antoine on 5/11/15.
 */
var app = angular.module('Commbank-test', ['ngRoute']).config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'table.html',
            controller: 'CommBankCtr'
        })
        .otherwise({redirectTo:'/'});
}]);


app.controller('CommBankCtr', function($scope, Currencies){

    $scope.selectedCurrency = "AUD";

    Currencies.fetchCurrencies().then(function(results) {
        $scope.currencies = results;
        console.log('currencies = ' );
        console.log($scope.currencies);
    });

});

app.service('Currencies', function($http,$q) {

    this.i = 0;

    this.startTime = new Date().getTime();

    this.currencyList = [
        {"path": "currency.AUD.json"},
        {"path": "currency.USD.json"},
        {"path": "currency.GBP.json"},
        {"path": "currency.ARS.json"},
        {"path": "currency.BRL.json"},
        {"path": "currency.CAD.json"},
        {"path": "currency.CLP.json"},
        {"path": "currency.CNY.json"},
        {"path": "currency.HRK.json"},
        {"path": "currency.CZK.json"},
        {"path": "currency.DKK.json"},
        {"path": "currency.EUR.json"},
        {"path": "currency.HKD.json"},
        {"path": "currency.HUF.json"},
        {"path": "currency.INR.json"},
        {"path": "currency.IDR.json"},
        {"path": "currency.ILS.json"},
        {"path": "currency.JPY.json"},
        {"path": "currency.KWD.json"},
        {"path": "currency.MYR.json"},
        {"path": "currency.MXN.json"},
        {"path": "currency.XPF.json"},
        {"path": "currency.NZD.json"},
        {"path": "currency.NOK.json"},
        {"path": "currency.OMR.json"},
        {"path": "currency.PLN.json"},
        {"path": "currency.QAR.json"},
        {"path": "currency.SAR.json"},
        {"path": "currency.SGD.json"},
        {"path": "currency.SBD.json"},
        {"path": "currency.ZAR.json"},
        {"path": "currency.KRW.json"},
        {"path": "currency.LKR.json"},
        {"path": "currency.SEK.json"},
        {"path": "currency.CHF.json"},
        {"path": "currency.TWD.json"},
        {"path": "currency.THB.json"},
        {"path": "currency.TRY.json"},
        {"path": "currency.AED.json"},
        {"path": "currency.VUV.json"},
        {"path": "currency.VND.json"},
        {"path": "currency.WST.json"}
    ];


    this.loading =- true;

    this.results = {};



    this.fetchCurrencies = function() {
        var deferred = $q.defer();
        var that = this;
        var counter = 0;
        var nbCurrencies = this.currencyList.length;

        for(var i in this.currencyList) {

            (function(index) {
                $http.get("assets/" + that.currencyList[index].path + '?cachebuster=' + that.startTime).then(function(msg) {

                    that.results[msg.data.countryCode] = msg.data;
                    counter++;
                    if(nbCurrencies === counter) {
                        console.log(that.results);
                        deferred.resolve(that.results);
                    }
                }, function(error) {
                    deferred.reject(error);
                });
            }(i));

        }

        return deferred.promise;

    };

    /*
     function populateTable (nextFile) {

     for (var j = 0; j < nextFile.length; j++) {

     var row = '<tr></tr>';

     row.innerHTML = '<td>' + nextFile[i].currencyName + '</td>' + '<td>' + nextFile[i].currencyTitle + '</td>' + '<td>' + nextFile[i].bbCashTChqs + '</td>' + '<td>' + nextFile[i].bbForeignCheques + '</td>' + '<td>' + nextFile[i].bbImt + '</td>' + '<td>' + nextFile[i].bsCashTmcTChqs + '</td>' + '<td>' + nextFile[i].bsImt + '</td>';

     table.appendChild(row);

     }

     }

     init();

     };
     */
});